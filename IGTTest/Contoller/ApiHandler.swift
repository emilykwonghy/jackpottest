//
//  ApiHandler.swift
//  IGTTest
//
//  Created by Emily Kwong on 10/3/2016.
//  Copyright © 2016 Emily Kwong. All rights reserved.
//

import Foundation
import UIKit

enum JsonError: ErrorType{
    case InvaildJsonFormat
}

class ApiHandler{
    let urlName = "https://dl.dropboxusercontent.com/u/49130683/nativeapp-test.json"
    let cachedJackpotFileName = "cachedGameData.json"
    let cacheTime: NSTimeInterval = 60 * 60
    
    func getData(completed completed: (data: AnyObject) -> Void) {
        if let data = loadFromCachdeData(){
            do{
                let parsedObject = try NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions())
                completed(data: parsedObject)
            }catch{
                JsonError.InvaildJsonFormat
            }
        }
        else {
            loadFromURL(completed: completed)
        }
    }
    
    func loadFromCachdeData() -> NSData? {
        let cachedHandler = CacheHandler()
        return cachedHandler.dataForCacheFile(cachedJackpotFileName)
    }
    
    func loadFromURL(completed completed: (data: AnyObject) -> Void) {
        let defaultSession = NSURLSession(configuration: NSURLSessionConfiguration.defaultSessionConfiguration());
        let url = NSURL(string: urlName)
        
        dispatch_async(dispatch_get_main_queue()) {
            UIApplication.sharedApplication().networkActivityIndicatorVisible = true
        }
        
        let dataTask = defaultSession.dataTaskWithURL(url!) {
            data, response, error in
            
            dispatch_async(dispatch_get_main_queue()){
                UIApplication.sharedApplication().networkActivityIndicatorVisible = false
            }
            
            if let error = error {
                print(error.localizedDescription)
            }
            else if let httpResponse = response as? NSHTTPURLResponse ,
                responseData = data {
                    if (httpResponse.statusCode == 200) {
                        do {
                            let parsedObject = try NSJSONSerialization.JSONObjectWithData(responseData, options: NSJSONReadingOptions())
                            completed(data: parsedObject)
                            
                            let cacheHandler = CacheHandler()
                            cacheHandler.updateCacheFile(responseData, fileName: self.cachedJackpotFileName, cacheTime: self.cacheTime)
                        } catch {
                            JsonError.InvaildJsonFormat
                        }
                    }
            }
        }
        dataTask.resume()
    }
}

