//
//  CacheHandler.swift
//  IGTTest
//
//  Created by Emily Kwong on 14/3/2016.
//  Copyright © 2016 Emily Kwong. All rights reserved.
//

import Foundation

class CacheHandler {
    let cacheJsonFileName = "cached.json"
    
    func dataForCacheFile(fileName: String) -> NSData? {
        if let cachedDict = self.cachedDict(),
          expiredtimeIntervalStr = cachedDict[fileName] as? String,
            let doubleValue = Double(expiredtimeIntervalStr)
        {
            let expiredtimeInterval = doubleValue as NSTimeInterval
            let expiredTime = NSDate(timeIntervalSince1970: expiredtimeInterval)
            let currentTime = NSDate()
            if (currentTime.compare(expiredTime) == NSComparisonResult.OrderedAscending){
                let fileHandler = FileHandler()
                return fileHandler.loadFromFile(fileName)
            }
            
        }
        return nil
    }
    
    func cachedDict() -> [String: AnyObject]? {
        let fileHandler = FileHandler()
        if let data = fileHandler.loadFromFile(cacheJsonFileName) {
            do {
                let parsedObject = try NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions())
                 return  parsedObject as? [String: AnyObject]
            }
            catch{
                return nil
            }
            
        }
        return nil
    }
    
    func updateCacheFile(data: NSData, fileName: String, cacheTime: NSTimeInterval) {
        var cachedDict:[String: AnyObject]
        if let dict = self.cachedDict(){
            cachedDict = dict
        }
        else {
            cachedDict = [:]
        }
        let currentTime = NSDate()
        let expiredTime = currentTime.dateByAddingTimeInterval(cacheTime)
        cachedDict[fileName] = String("\(expiredTime.timeIntervalSince1970)")
        
        let fileHandler = FileHandler()
        fileHandler.savetoTextFile(cacheJsonFileName, dict: cachedDict)
        fileHandler.saveToTextFile(fileName, data: data)
    }
}