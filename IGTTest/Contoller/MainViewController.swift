//
//  MasterViewController.swift
//  IGTTest
//
//  Created by Emily Kwong on 10/3/2016.
//  Copyright © 2016 Emily Kwong. All rights reserved.
//

import UIKit

class MainViewController: UITableViewController {
    var detailViewController: DetailViewController? = nil
    var objects = [AnyObject]()


    override func viewDidLoad() {
        super.viewDidLoad()

        requestDataFromManager(nil)
    }

    func requestDataFromManager(sender: AnyObject?){
        JackpotManager.sharedManager.requestJackPotArray(){
            
            dispatch_async(dispatch_get_main_queue()){
                self.reloadTable()
                if let refreshControl = sender as? UIRefreshControl{
                    refreshControl.endRefreshing()
                }
            }
        }
    }
    
    func reloadTable(){
        self.tableView.reloadData();
    }

    // MARK: - Segues

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "show" {
            if let indexPath = self.tableView.indexPathForSelectedRow {
                let object = JackpotManager.sharedManager.jackpotArray[indexPath.row]
                let controller = segue.destinationViewController as! DetailViewController
                controller.detailItem = object
            }
        }
    }

    // MARK: - Table View

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return JackpotManager.sharedManager.jackpotArray.count
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath)

//        let object = objects[indexPath.row] as! NSDate
        
        let jackpot = JackpotManager.sharedManager.jackpotArray[indexPath.row]
        cell.textLabel!.text = jackpot.localeName
        
        return cell
    }

    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }

    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            objects.removeAtIndex(indexPath.row)
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
        }
    }

    @IBAction func refresh(sender: AnyObject) {
        requestDataFromManager(sender)
    }
}

