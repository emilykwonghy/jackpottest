//
//  File.swift
//  IGTTest
//
//  Created by Emily Kwong on 11/3/2016.
//  Copyright © 2016 Emily Kwong. All rights reserved.
//

import Foundation

class JackpotManager {
    static let sharedManager = JackpotManager()
    
    var jackpotArray : [Jackpot]! = []
    var currency : String?
    
    func requestJackPotArray(completed: () -> Void){
        dispatch_async(dispatch_get_global_queue(Int(QOS_CLASS_USER_INITIATED.rawValue), 0)){
            
            let apiHandler = ApiHandler()
            apiHandler.getData(){
                data in
                do {
                    try self.parseJson(data)
                    completed()
                }
                catch {
                    
                }
                
            }
        }
    }
    
    func updateArray(jackpotArray: [Jackpot]){
        self.jackpotArray.removeAll()
        self.jackpotArray = jackpotArray
    }
    
    func parseJson(jsonObject: AnyObject) throws{
        guard let jsonDict = jsonObject as? [String: AnyObject],
            array = jsonDict["data"] as? [AnyObject],
        currency = jsonDict["currency"] as? String
        else {
            throw JsonError.InvaildJsonFormat
        }
        
        var jackpotArray: [Jackpot] = Array()
        for item in array{
            
            guard let itemData = item as? [String: AnyObject] else {
                return
            }
            
            guard let name = itemData["name"] as? String,
                jackpotAmount = item["jackpot"] as? Int,
                dateString = item["date"] as? String else {
                    return
            }
            
            let df = NSDateFormatter()
            df.dateFormat = ("yyyy-MM-dd'T'HH:mm:ssZZZZZ")
            if let date = df.dateFromString(dateString){
                
                let jackpot = Jackpot(name: name, jackpotAmount: jackpotAmount, date: date)
                jackpotArray.append(jackpot)
                //                JackpotManager.sharedManager.jackpotArray.append(jackpot);
                
            }
        }
        self.currency = currency
        updateArray(jackpotArray)
    }
}