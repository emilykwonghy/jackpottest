//
//  DetailViewController.swift
//  IGTTest
//
//  Created by Emily Kwong on 10/3/2016.
//  Copyright © 2016 Emily Kwong. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var jackpotAmountLabel: UILabel!
    @IBOutlet weak var timeLabel:UILabel!


    var detailItem: AnyObject? {
        didSet {
            // Update the view.
            self.configureView()
        }
    }

    func configureView() {
        // Update the user interface for the detail item.
        if let jackpot = self.detailItem as? Jackpot{

            if let label = self.nameLabel {
                label.text = jackpot.localeName
            }
            
            if let label = self.jackpotAmountLabel{
                label.text = jackpot.localeJackpotAmount
            }
            
            if let label = self.timeLabel{                
                label.text = jackpot.localeDate
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.configureView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

