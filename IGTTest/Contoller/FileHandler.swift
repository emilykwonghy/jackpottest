//
//  FileHandler.swift
//  IGTTest
//
//  Created by Emily Kwong on 13/3/2016.
//  Copyright © 2016 Emily Kwong. All rights reserved.
//

import Foundation

enum FileHandlerError: ErrorType {
    case pathNotExist
    case cannotCreateFile
}

class FileHandler {
    
    let fileManager = NSFileManager.defaultManager()
    
    func documentDirectory() throws -> String {
        
        let documentsPaths = NSSearchPathForDirectoriesInDomains(.LibraryDirectory, .UserDomainMask, true)
        guard let docPath = documentsPaths.first else{
            // error, path not exist
            throw FileHandlerError.pathNotExist
        }
        
//        add private documents
        let pathURL = NSURL(fileURLWithPath: docPath, isDirectory: true)
        guard let privatePath = pathURL.URLByAppendingPathComponent("Private_Doc").path else {
            throw FileHandlerError.cannotCreateFile
        }
        var isDirectory = ObjCBool(true)
        
        if !fileManager.fileExistsAtPath(privatePath, isDirectory: &isDirectory) {
            try fileManager.createDirectoryAtPath(privatePath, withIntermediateDirectories: true, attributes: nil)
        }
        return privatePath
    }
    
    func saveToTextFile(fileName: String, data: NSData){
        
        do {
            let documentPath = try documentDirectory()
            let documentURL = NSURL(fileURLWithPath: documentPath)
            guard let dataPath = documentURL.URLByAppendingPathComponent(fileName).path else {
                return 
            }
            if fileManager.fileExistsAtPath(dataPath) {
                try data.writeToFile(dataPath, options:.DataWritingAtomic)
            } else {
                guard fileManager.createFileAtPath(dataPath, contents: data, attributes: nil) else {
                    throw FileHandlerError.cannotCreateFile
                }
            }
            
        } catch {
            print(error)
        }
    }
    
    func savetoTextFile(fileName: String, dict: [String: AnyObject]){
        do {
            let jsonData = try NSJSONSerialization.dataWithJSONObject(dict, options: .PrettyPrinted)
            saveToTextFile(fileName, data: jsonData)
        }
        catch {
            
        }
    }
    
    
    
    func loadFromFile(fileName: String) -> NSData?{
        do {
            let documentPath = try documentDirectory()
            let documentURL = NSURL(fileURLWithPath: documentPath)
            
            guard let fullPath = documentURL.URLByAppendingPathComponent(fileName).path else {
                return nil
            }
            let fileManager = NSFileManager.defaultManager()

            if fileManager.fileExistsAtPath(fullPath)
            {
                let jsonData = try NSData(contentsOfFile: fullPath, options: .DataReadingMappedIfSafe)

                return jsonData;
            }
            
        }
        catch{
            print(error)
        }
        return nil
    }
}