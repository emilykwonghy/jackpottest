//
//  Jackpot.swift
//  IGTTest
//
//  Created by Emily Kwong on 10/3/2016.
//  Copyright © 2016 Emily Kwong. All rights reserved.
//

import Foundation

class Jackpot{
    private var name: String
    private var jackpotAmount: Int
    private var date: NSDate
    
    init(name: String, jackpotAmount: Int, date: NSDate) {
        self.name = name
        self.jackpotAmount = jackpotAmount
        self.date = date
    }
    
    var localeName: String {
        get {
            return NSLocalizedString(name, comment: "Game Name")
        }
    }
    
    var localeJackpotAmount: String? {
        get {
            let currencyFormatter = NSNumberFormatter()
            currencyFormatter.numberStyle = NSNumberFormatterStyle.CurrencyStyle
            currencyFormatter.currencyCode = JackpotManager.sharedManager.currency
            if let str = currencyFormatter.stringFromNumber(jackpotAmount){
                return str
            }
            return nil
        }
    }
    
    var localeDate: String {
        get {
            let dateString = NSDateFormatter.localizedStringFromDate(date, dateStyle: .LongStyle, timeStyle: .MediumStyle)
            return dateString
        }
    }
}